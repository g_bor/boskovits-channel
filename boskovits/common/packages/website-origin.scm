(define-module (boskovits common packages website-origin)
  #:export (make-website-origin)
  #:use-module (guix git-download)
  #:use-module (guix packages))

(define* (make-website-origin
	  #:key
	  name
	  uri
	  commit
	  hash)
  `(origin
    (method git-fetch)
    (uri (git-reference
	  (url ,uri)
	  (commit ,commit)))
    (sha256
     (base32 ,hash))
    (file-name (git-file-name ,name ,commit))))
